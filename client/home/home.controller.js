//home.controller.js

angular.module('contability.home')

.controller('homeController', homeController)

homeController.$inject = ['$scope', 'months', 'resource']

function homeController($scope, months, resource) {
  var home = this;
  home.months = months.values;
  home.spread = 1.05;

  home.genTables = genTables;
  home.genSpreadTable = genSpreadTable;
  home.update = update;

  function genTables() {
    home.tables = resource.genStaticTable(home.spread);
  }

  function genSpreadTable() {

    var args = {
      dom: 'spread',
      column: {
        type: 'number',
        name: home.spread
      },
      width: '200px'
    }

    resource.genSpreadTable(args)
  }

  function update() {
    console.log("update");
  }

  home.genSpreadTable()
  home.genTables()

}
