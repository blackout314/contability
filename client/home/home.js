//home.js

angular.module('contability.home', ['ngAnimate', 'contability.resource', 'contability.constants'])
.constant('table', {
  args: {
    columns: [{
      type: 'string',
      name: 'Nome'
    }, {
      type: 'number',
      name: 'f1'
    }, {
      type: 'number',
      name: 'f2'
    }, {
      type: 'number',
      name: 'f3'
    }],
    rows: [
      ['Mike', {
        v: 10000,
        f: '$10,000'
      }, {
        v: 10000,
        f: '$10,000'
      }, {
        v: 10000,
        f: '$10,000'
      }],
      ['Jim', {
        v: 8000,
        f: '$8,000'
      }, {
        v: 10000,
        f: '$10,000'
      }, {
        v: 10000,
        f: '$10,000'
      }],
      ['Alice', {
        v: 12500,
        f: '$12,500'
      }, {
        v: 10000,
        f: '$10,000'
      }, {
        v: 10000,
        f: '$10,000'
      }],
      ['Bob', {
        v: 7000,
        f: '$7,000'
      }, {
        v: 10000,
        f: '$10,000'
      }, {
        v: 10000,
        f: '$10,000'
      }]
    ],
    width: '250px',
  }
})
