//config.js

angular.module('contability.config', ['ngRoute', 'ui.router', 'ui.router.stateHelper', 'contability.home', 'contability.directives'])

.config(config)

config.$inject = ['$httpProvider', '$urlRouterProvider', '$stateProvider', 'stateHelperProvider']

function config($httpProvider, $urlRouterProvider, $stateProvider, stateHelperProvider) {

  $urlRouterProvider.otherwise("/home");

  stateHelperProvider

    .state({
    name: 'home',
    url: "/home",
    templateUrl: '/home/tables.html',
    controller: "homeController as home"
  })

}
