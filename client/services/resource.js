angular.module('contability.resource', ['contability.constants'])

.service('resource', resource)

resource.$inject = ['months']

function resource(months) {

  const values = months.values;
  const max = 70;

  var service = {
    randNum, randNum,
    genStaticTable: genStaticTable,
      genSpreadTable: genSpreadTable
  }

  return service;

  function randNum() {
    return Math.floor((Math.random() * max) + 1);
  }

  function genStaticTable(spread) {

    const limit = 1000;
    var tables = {}
    _.forEach(values, function(month) {
      tables[month] = {}
      tables[month].f1 = randNum();
      tables[month].f2 = randNum();
      tables[month].f3 = randNum();

      tables[month].prb1 = randNum();
      tables[month].prb2 = randNum();
      tables[month].prb3 = randNum();

      tables[month].prf1 = tables[month].prb1 + parseInt(spread);
      tables[month].prf2 = tables[month].prb2 + parseInt(spread);
      tables[month].prf3 = tables[month].prb3 + parseInt(spread);

      var f1 = tables[month].f1;
      var f2 = tables[month].f2;
      var f3 = tables[month].f3;
      var prb1 = tables[month].prb1;
      var prb2 = tables[month].prb2;
      var prb3 = tables[month].prb3;
      tables[month].imp = parseFloat((f1 * prb1 / limit) + (f2 * prb2 / limit) + (f3 * prb3 / limit)).toFixed(2);
    })

    return tables;
  }

  function genSpreadTable(args) {

    function drawChart() {

      function selectHandler() {
        console.log(args)
      }

      var dom = args.dom;
      var column = args.column;
      var width = args.width;
      //console.log(args)

      var data = new google.visualization.DataTable();
      data.addColumn(column.type, column.name);

      var el = document.getElementById(dom);
      var table = new google.visualization.Table(el);

      table.draw(data, {
        width: width
      })

    }

    google.charts.load('visualization', '41', {
      packages: ['gauge', 'table']
    })
    google.charts.setOnLoadCallback(drawChart)
  }
}
