//constants.js

angular.module('contability.constants', [])

.constant('months', {
    values: ['gen', 'feb', 'mar', 'arp', 'mag', 'giu', 'lug', 'ago', 'set', 'ott', 'nov', 'dic']
  })
