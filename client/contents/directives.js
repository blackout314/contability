angular.module('contability.directives', ['contability.headerbar'])

.directive('headerBar', function() {
  return {
    restrict: 'AE',
    controller: 'headerbarController',
    controllerAs: 'headerbar',
    templateUrl: '../layout/headerbar.html'
  };
})
