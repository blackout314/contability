function getResponse(req, res, next) {
  var responseText = 'resource ';
  res.send(responseText);
  return next();
}

module.exports.getResponse = getResponse;
