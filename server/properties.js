'use strict';
/**
 * Globally used properties.
 */

/* Third party imports */

module.exports = {
  server_name: 'Contability',
  port: 3000,
}
